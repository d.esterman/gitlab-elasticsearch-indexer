module gitlab.com/gitlab-org/gitlab-elasticsearch-indexer

require (
	github.com/aws/aws-sdk-go v1.19.6
	github.com/deoxxa/aws_signing_client v0.0.0-20161109131055-c20ee106809e
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/olivere/elastic v6.2.24+incompatible
	github.com/sirupsen/logrus v1.4.1
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a // indirect
	github.com/stretchr/testify v1.3.0
	gitlab.com/gitlab-org/gitaly v1.68.0
	gitlab.com/lupine/icu v1.0.0
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	google.golang.org/grpc v1.24.0
	gopkg.in/ini.v1 v1.42.0 // indirect
)
